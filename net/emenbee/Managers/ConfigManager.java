package net.emenbee.Managers;

import net.emenbee.Main.ETownyFly;

public class ConfigManager
{
    private ETownyFly getMain = ETownyFly.getMain();

    public String getNoPermissionMessage()
    {
        return getMain.getConfig().getString("NoPermission");
    }

    public String getPrefix()
    {
        return getMain.getConfig().getString("Prefix");
    }

    public String getFlyEnabledMessage()
    {
        return getMain.getConfig().getString("Fly.Enabled");
    }

    public String getFlyDisabledMessage()
    {
        return getMain.getConfig().getString("Fly.Disabled");
    }

    public String getNoTownFoundMessage()
    {
        return getMain.getConfig().getString("NoTownFound");
    }

    public String getInWildernessMessage()
    {
        return getMain.getConfig().getString("InWilderness");
    }

    public String getNotInJoinedTownMessage()
    {
        return getMain.getConfig().getString("NotInJoinedTown");
    }
}
