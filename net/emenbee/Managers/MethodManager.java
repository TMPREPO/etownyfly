package net.emenbee.Managers;

import net.emenbee.Main.ETownyFly;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class MethodManager
{
    private ETownyFly getMain = ETownyFly.getMain();
    private ConfigManager getCfg = ETownyFly.getCfgManager();

    public void setFly(Player p, boolean bool)
    {
        p.setAllowFlight(bool);
        p.setFlying(bool);

        if (bool == false) {
            getMain.players.remove(p.getName());
            if (p.getLocation().subtract(0, 1, 0).getBlock().getType() == Material.AIR) {
                getMain.nofall.add(p.getName());
            }
        }

        if (bool == true) {
            getMain.players.add(p.getName());
        }
    }

    public void sMsg(Player p, String str)
    {
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', str
        .replace("<prefix>", getCfg.getPrefix())));
    }
}
