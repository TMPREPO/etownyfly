package net.emenbee.Events;

import com.palmergames.bukkit.towny.event.PlayerChangePlotEvent;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownBlock;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import net.emenbee.Main.ETownyFly;
import net.emenbee.Managers.ConfigManager;
import net.emenbee.Managers.MethodManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlotChange implements Listener
{
    private ETownyFly getMain = ETownyFly.getMain();
    private MethodManager getMethods = ETownyFly.getMethManager();
    private ConfigManager getCfg = ETownyFly.getCfgManager();
    private Resident res;
    private TownBlock tb;
    private Town townTo;

    @EventHandler
    public void onPlotChange(PlayerChangePlotEvent e)
    {
        Player p = e.getPlayer();
        if (!getMain.players.contains(p.getName())) {
            return;
        }

        try
        {
            res = TownyUniverse.getDataSource().getResident(p.getName());
        } catch (NotRegisteredException nre) {
            getMain.log("**********ERROR, COULD NOT GET RESIDENT " + p.getName() + " CORRECTLY, CAUSED BY " + nre.getMessage() + " **********");
        }


        try
        {
            tb = e.getTo().getTownBlock();
            if (tb != null) {
                townTo = tb.getTown();
                if (res.getTown() != townTo) {
                    getMethods.sMsg(p, getCfg.getNoTownFoundMessage());
                    getMethods.setFly(p, false);
                }
                return;
            }
        } catch (NotRegisteredException nre2) {
            getMethods.sMsg(p, getCfg.getInWildernessMessage());
            getMethods.setFly(p, false);
            return;
        }
        return;
    }
}