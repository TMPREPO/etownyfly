package net.emenbee.Events;

import net.emenbee.Main.ETownyFly;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class CancelDamage implements Listener
{
    private ETownyFly getMain = ETownyFly.getMain();

    @EventHandler
    public void onDamage(EntityDamageEvent e) throws NullPointerException
    {
        if (!(e.getEntity() instanceof Player)) {
            return;
        }

        Player p = (Player)e.getEntity();

        if (!getMain.nofall.contains(p.getName())) {
            return;
        }

        if (e.getCause().equals(EntityDamageEvent.DamageCause.FALL) || p.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
            e.setCancelled(true);
            getMain.nofall.remove(p.getName());
            return;
        }
        return;
    }
}
