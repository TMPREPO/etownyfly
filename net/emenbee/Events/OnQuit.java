package net.emenbee.Events;

import net.emenbee.Main.ETownyFly;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class OnQuit implements Listener
{
    private ETownyFly getMain = ETownyFly.getMain();

    @EventHandler
    public void onQuit(PlayerQuitEvent e)
    {
        Player p = e.getPlayer();

        Location pLoc = p.getLocation().clone();
        int loc = pLoc.getBlockY();

        for (int i = p.getLocation().getBlockY(); i > 0; i--) {
            Location locit = new Location(p.getWorld(), pLoc.getX(), i, pLoc.getZ());

            if (locit.getBlock().getType() != Material.AIR) {
                p.teleport(locit.add(0, 1, 0));
                return;
            }
        }
    }
}
