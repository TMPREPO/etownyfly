package net.emenbee.Main;

import net.emenbee.Events.CancelDamage;
import net.emenbee.Events.OnQuit;
import net.emenbee.Events.PlotChange;
import net.emenbee.Managers.ConfigManager;
import net.emenbee.Managers.MethodManager;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;

public class ETownyFly extends JavaPlugin
{
    private static ETownyFly maininstace;
    private static ConfigManager cfgmanagerinstance;
    private static MethodManager methmanagerinstance;
    private PluginManager pm = getServer().getPluginManager();
    private File cfgf;
    private YamlConfiguration cfg;
    public ArrayList<String> players;
    public ArrayList<String> nofall;

    @Override
    public void onEnable()
    {
        hookTowny();
        maininstace = this;
        cfgmanagerinstance = new ConfigManager();
        methmanagerinstance = new MethodManager();
        players = new ArrayList<>();
        nofall = new ArrayList<>();
        pm.registerEvents(new CancelDamage(), this);
        pm.registerEvents(new PlotChange(), this);
        pm.registerEvents(new OnQuit(), this);
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        log("Is now enabled.");
        getCommand("tfly").setExecutor(new CommandHandler());
    }

    public void onDisable()
    {
        players.clear();
        nofall.clear();
        log("Is now disabled.");
    }

    public void log(String str)
    {
        System.out.println("[ETownyFly] " + str);
    }

    public static ETownyFly getMain()
    {
        return maininstace;
    }

    public static ConfigManager getCfgManager()
    {
        return cfgmanagerinstance;
    }

    public static MethodManager getMethManager()
    {
        return methmanagerinstance;
    }

    private void hookTowny()
    {
        if (pm.getPlugin("Towny") == null) {
            log("Could not find the Towny plugin, is it installed?? Disabling plugin.");
            pm.disablePlugin(this);
            return;
        }
    }
}
