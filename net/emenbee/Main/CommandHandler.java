package net.emenbee.Main;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import net.emenbee.Managers.ConfigManager;
import net.emenbee.Managers.MethodManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHandler implements CommandExecutor
{
    private ETownyFly getMain = ETownyFly.getMain();
    private ConfigManager getCfg = ETownyFly.getCfgManager();
    private MethodManager getMethods = ETownyFly.getMethManager();
    private Resident res;

    @Override
    public boolean onCommand(CommandSender s, Command cmd, String str, String[] args)
    {
        if (!cmd.getName().equalsIgnoreCase("tfly")) {
            return true;
        }

        if (!(s instanceof Player)) {
            return true;
        }

        Player p = (Player)s;

        if (!p.hasPermission("emenbee.townyfly")) {
            getMethods.sMsg(p, getCfg.getNoPermissionMessage());
            return true;
        }

        try
        {
            res = TownyUniverse.getDataSource().getResident(p.getName());
        } catch (NotRegisteredException nre) {
            getMain.log("**********ERROR, COULD NOT GET RESIDENT " + p.getName() + " CORRECTLY, CAUSED BY " + nre.getMessage() + " **********");
        }

        if (!res.hasTown()) {
            getMethods.sMsg(p, getCfg.getNoTownFoundMessage());
            return true;
        }

        if (TownyUniverse.isWilderness(p.getLocation().getBlock())) {
            getMethods.sMsg(p, getCfg.getInWildernessMessage());
            return true;
        }

        try
        {
            if (!TownyUniverse.getTownBlock(p.getLocation()).getTown().equals(res.getTown())) {
                getMethods.sMsg(p, getCfg.getNotInJoinedTownMessage());
                return true;
            }
        } catch (NotRegisteredException nre2) {
            nre2.printStackTrace();
        }

        try
        {
            if (p.getAllowFlight() == false && !getMain.players.contains(p.getName())) {
                getMethods.sMsg(p, getCfg.getFlyEnabledMessage());
                getMethods.setFly(p, true);
            } else {
                getMethods.sMsg(p, getCfg.getFlyDisabledMessage());
                getMethods.setFly(p, false);
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }

        return true;
    }
}
